import 'package:flutter/material.dart';

//main function is the starting point of the project
void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.yellow,
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: const Text("Project ABC"),
        ),
        body: const Center(
          child: Image(
            image: AssetImage(
                "assets/images/image_abc.jpeg"),
          ),
        ),
      ),
    ),
  );
}
